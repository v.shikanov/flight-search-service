Requirements
------------
 - PHP >= 7.1.3
 - Laravel >= 5.5.0

Installation
------------

Run containers:
```
docker-compose up -d
```

Enter the workspace container:
```
docker-compose exec workspace bash
```

Create your local ```.env``` config:  
```
cp .env.example .env
```
```
php atisan key:generate
```

Install dependencies:
```
composer install
```

Run migrations:
```
php artisan migrate --seed
```
```
php artisan migrate --seed --env=testing
```

Usage
----------
Access the application in your browser at ```http://localhost:8111```.


Tests
-----

Run ```composer test``` inside ```workspace``` container
