<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Airline
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Airline query()
 * @mixin \Eloquent
 */
class Airline extends Model
{
    //
}
