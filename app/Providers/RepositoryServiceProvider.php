<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\RouteRepositoryInterface;
use App\Repositories\RouteRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->app->bind(RouteRepositoryInterface::class, RouteRepository::class);
    }
}
