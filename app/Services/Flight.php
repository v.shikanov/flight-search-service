<?php

namespace App\Services;

use App\Repositories\RouteRepositoryInterface;

class Flight
{
    /**
     * @var \App\Repositories\RouteRepositoryInterface
     */
    private $routeRepository;

    /**
     * @param RouteRepositoryInterface $routeRepository
     */
    public function __construct(RouteRepositoryInterface $routeRepository)
    {
        $this->routeRepository = $routeRepository;
    }

    /**
     * @return mixed
     */
    public function search()
    {
        return $this->routeRepository
            ->with(['airline', 'departureAirport', 'arrivalAirport'])
            ->paginate();
    }
}
