<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Search as SearchRequest;
use App\Services\Flight as FlightService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use Exception;

class SearchController extends Controller
{
    /**
     * @var \App\Services\Flight
     */
    protected $flightService;

    /**
     * @param  FlightService $flightService)
     */
    public function __construct(FlightService $flightService)
    {
        $this->flightService = $flightService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            return $this->flightService->search();
        } catch (ValidationException $validationException) {
            response()->json([
                'status' => false,
                'message' => $validationException->getMessage(),
            ]);
        } catch (Exception $exception) {
            response()->json([
                'status' => false,
                'message' => 'Something went wrong, please try again.',
            ]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
