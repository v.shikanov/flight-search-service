<?php

namespace Tests\Feature\Api;

use App\Models\Airport;
use App\Models\Route as RouteModel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SearchTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSearchTest(): void
    {
        $datetime = now(new \DateTimeZone('UTC'));
        $datetimeFormatted = $datetime->toFormattedDateString();

        factory(RouteModel::class, 3)
            ->create([
                'departure_datetime' => $datetime,
                'departure_airport_id' => function () {
                    return factory(Airport::class)->create([
                        'iata_code' => 'KBP',
                    ])->id;
                },
                'arrival_airport_id' => function () {
                    return factory(Airport::class)->create([
                        'iata_code' => 'BUD',
                    ])->id;
                },
            ]);

        $response = $this->json('GET', route('api.search'), [
            'search' => "departureAirport.iata_code:KBP;arrivalAirport.iata_code:BUD;departure_datetime=$datetimeFormatted",
        ], [
            'Authorization' => 'Bearer test',
        ]);

        $response->assertStatus(200);
        $response->assertJsonFragment(['total' => 3]);
    }
}
