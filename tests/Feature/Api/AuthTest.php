<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicAuthTest(): void
    {
        $response = $this->getJson(route('api.user'), [
            'Authorization' => 'Bearer test',
        ]);

        $response->assertStatus(200);
    }
}
