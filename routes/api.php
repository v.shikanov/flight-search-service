<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api',
    //'middleware' => 'auth:api',
], function () {
    Route::name('api.')->group(function () {

        Route::get('/user', function (Request $request) {
            return $request->user();
        })->name('user');

        Route::get('/search', 'SearchController@index')->name('search');
    });
});
