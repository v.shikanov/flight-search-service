<?php

use Illuminate\Database\Seeder;
use App\Models\Airport as AirportModel;

class AirportsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AirportModel::class)->create();
    }
}
