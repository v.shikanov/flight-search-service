<?php

use Illuminate\Database\Seeder;
use App\Models\Airline as AirlineModel;

class AirlinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AirlineModel::class)->create();
    }
}
