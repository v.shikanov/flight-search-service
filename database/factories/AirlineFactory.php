<?php

use Faker\Generator as Faker;
use App\Models\Airline as AirlineModel;
use Illuminate\Support\Str;

$factory->define(AirlineModel::class, function (Faker $faker) {
    return [
        'iata_code' => Str::random(2),
        'icao_code' => Str::random(3),
        'name' => $faker->company,
    ];
});
