<?php

use Faker\Generator as Faker;
use App\Models\Airport as AirportModel;
use Illuminate\Support\Str;

$factory->define(AirportModel::class, function (Faker $faker) {
    return [
        'continent' => Str::random(2),
        'latitude'  => 30.894699096679688,
        'longitude' => 50.345001220703125,
        'iata_code' => strtoupper(Str::random(3)),
        'iso_country' => Str::random(2),
        'iso_region' => $faker->randomLetter,
        'name' => "Airport #$faker->randomDigit",
    ];
});
