<?php

use Faker\Generator as Faker;
use App\Models\Route as RouteModel;

$factory->define(RouteModel::class, function (Faker $faker) {
    return [
        'airline_id' => function () {
            return factory(\App\Models\Airline::class)->create()->id;
        },
        'departure_airport_id' => function () {
            return factory(\App\Models\Airport::class)->create()->id;
        },
        'arrival_airport_id' => function () {
            return factory(\App\Models\Airport::class)->create()->id;
        },
        'departure_datetime' => now(),
        'arrival_datetime' => now()->addHours(8),
        'duration' => 8*60,
        'flightNumber' => $faker->randomAscii . $faker->randomDigit,
    ];
});
